import React from 'react'; //must need for every js in src 
import ReactDOM from 'react-dom'; //only use when rendering with the DOM
import './index.css'; //linking css in react; index.css gets all other css merged into it
import App from './App'; //including a different js into another; importing the App.js in here
import * as serviceWorker from './serviceWorker'; //optional


ReactDOM.render( //this is the main source 
  <React.StrictMode> 
    <App />
  </React.StrictMode>,
  document.getElementById('root')  //is from the public/index.html
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
