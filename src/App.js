import React from 'react'; //how it knows it is a react app
import ReactDOM from 'react-dom'; //how it is able to communicate with the DOM
import './App.css'; //linking css

function App() { //pure function
  const numberRef = React.useRef() //keyword to allow grabbing values easier check line 73
  const snap = [] //variable of empty array HINT: this could be set as state..
  
  //method for handling Submit from form; must include a parameter for the event
  const handleSubmit = (event) => {
    event.preventDefault(); //make sure page doesn't auto reload immediately
    let outputSource = document.getElementById('output') 
    let number = numberRef.current.value //remember line 6 and 73 now we can grab that value ; must have current before value
    console.log(number)
    let output1 = snapCrackle(number);
    let output2 = snapCracklePrime(snap);
    console.log(output1)
    console.log(output2)
    //similar to document.createElement but must create a React element; takes parameters
    //first the element you want to create
    //next any attribute for that element (className or id....)
    //next will be the content; here we created more elements inside of this element
    //reason for more elements was this was best way we say to put on new line
    let div = React.createElement(
      'div',
      {className: 'output'},
      React.createElement('div', {id:output1}, `SnapCrackle list: ${output1}`),
      React.createElement('div', {id:output2}, `SnapCracklePrime list: ${output1}`)
    );
    console.log(div)
    //similar to document.body.appendChild where ReactDOM is document.body and appendChild is render but render takes 
    //two parameters first what we are adding and second where we are adding to
    ReactDOM.render(div, outputSource ); 
  }

  //method for finding Snaps Crackles and Pops
  const snapCrackle = (maxValue) => {
    for (let i = 1; i <= maxValue; i++) {
      if ((i % 2) && !(i % 5)) {
        snap.push('Pop');
      } else if (!(i % 5)) {
        snap.push('Crackle');
      } else if (i % 2) {
        snap.push('Snap');
      } else {
        snap.push(i);
      }
    }
    // console.log(snap.toString());
    let snapcrackle = snap.join(',  ');
    return snapcrackle;
  }
   //method for finding Snaps Crackles and Pops that are also Prime
  const snapCracklePrime = (value) => { 
    let primes = [1];
    for (let i = 2; i <= value.length; i++) {
      let dividisble = (factorial(i - 1) + 1);
      if (!(dividisble % i)) {
        primes.push('Prime');
      } else {
        primes.push(i);
      }
    }
    //console.log(primes);
    for (let j = 0; j < primes.length; j++) {
      if (primes[j] === "Prime") {
        snap[j] = snap[j] + "-Prime"
      }
    }
    // console.log(snap.toString());
    let snapcrackle = snap.join(',  ');
    return snapcrackle;
  }
   //method for finding Primes
  const factorial = (num1) => { 
    let totalFact = num1;
    for (let i = num1 - 1; i >= 1; i--) {
        totalFact = totalFact * i;
    }
    return totalFact;
}
  return ( //must need return () to put elements on the page
  //similar to a div but mainly just a small piece that acts as a single parent
  //multiple divs will be allowed in here; return must have only one PARENT
    <React.Fragment> 
    <div className="App">
      <form onSubmit={handleSubmit}>
        <input ref={numberRef} id="num" type='text' placeholder='Enter number' />
        <button type='submit'>Snap, Crackle, SnapCrackle</button>
      </form>
    </div>
    <div id='output'></div>
    </React.Fragment>
  );
}

export default App; //similar to calling a function in javascript but here we are exporting it to be use throughout the folder
